Pod::Spec.new do |s|
    s.name         = "MobilePay"
    s.version      = "3.0.1"
    s.summary      = "A brief description of MobilePay project."
    s.description  = <<-DESC
    An extended description of MobilePay project.
    DESC
    s.homepage     = "http://justtappit.com"
    s.license = { :type => 'Copyright', :text => <<-LICENSE
                   Copyright 2018
                   Permission is granted to...
                  LICENSE
                }
    s.author             = { "$(git config user.name)" => "$(git config user.email)" }
    s.source       = { :git => "https://gitlab.com/tappitmobilepay/iosmobilepaypaygbinary.git", :tag => "#{s.version}" }
    s.vendored_frameworks = "MobilePay.xcframework"
    s.platform = :ios
    s.swift_version = "5.1"
    s.ios.deployment_target  = '11.4'
end